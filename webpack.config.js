
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './static/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: './static/index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js'
  },
  node: {
    fs: 'empty',
  },
  module: {
    rules: [
      { test: /\.js$/, loader: 'babel-loader', exclude: '/node_modules/' },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: '/node_modules/' }
    ]
  },
  plugins: [HtmlWebpackPluginConfig]
}

