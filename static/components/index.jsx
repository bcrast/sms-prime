import React from 'react';
import request from 'request';

// status component that essentially formats properties passed down from its parent component
function Status(props) {
  if (props.loading) {
    return <p key="loading">Calculating prime factors</p>;
  }
  else if(props.error) {
    return <p key="error" style={{color: "red"}}>Something went wrong</p>;
  }
  else if(props.primes) {
    return <p key="primes">{props.primes}</p>;
  }
  return '';
}


class PrimeButton extends React.Component {
  constructor(props) {
    super();
    this.state = {}
    this.getPrimeFactors = this.getPrimeFactors.bind(this);
  }

  getInitialState() {
    return {
      loading: false,
      primeFactors: '',
      error: false,
    }
  }

  //make request to primes endpoint, provide status along the way to the user
  getPrimeFactors(){
    this.setState({loading: true, error: false, primeFactors: '' });
    fetch('primes').then(resp => {
      const res = resp.text();
      res.then(result => {
        this.setState({ primeFactors: result });
      });
    }).catch(er => {
      this.setState({error: true});
      //log error for debug purposes
      //console.log(er);
    }).finally(() => {
        this.setState({loading: false});
      });
  };

  //render button and status component
  render() {
    return (
      <div>
        <button key="button"onClick={this.getPrimeFactors}>
          Get Prime Factors
        </button>
        <Status primes={this.state.primeFactors} loading={this.state.loading} error={this.state.error} />
      </div>
      );
  }
}

export default class App extends React.Component {
  render() {
    return <PrimeButton />;
  }
};


