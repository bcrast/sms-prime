const express = require('express')
const Rx = require('rxjs')
const request = require('request')
const app = express()

app.use(express.static('dist'))

//create observable from http request
function httpObservable(url, key) {
  return Rx.Observable.create(function(observer) {
    request.get(url, (er, resp, body) => {
      const json = JSON.parse(body)
      observer.next(parseInt(json[key]))
      observer.complete()
    }) 
  })
}

// get list of prime factors
function primeFactorList(n) {
	const result = []
	while (n != 1) {
		const factor = smallestFactor(n)
		result.push(factor)
		n /= factor;
	}
	return result;
}

// gets the smallest prime factor of the given integer.
function smallestFactor(n) {
	if (n % 2 == 0)
		return 2
	const end = Math.floor(Math.sqrt(n))
	for (let i = 3; i <= end; i += 2) {
		if (n % i == 0)
			return i
	}
	return n;
}

app.get('/primes', (req, res) => {
  const url$ = [
    httpObservable('http://dashboard.savemysales.co/devtest/1', 'val1'),
    httpObservable('http://dashboard.savemysales.co/devtest/2', 'val_2')
  ]
  // get list of observables, wait for each one to complete (forkJoin),
  // then add up the results, get the prime factor list and return
  // format it appropriately, and return it to the frontend
  const ob$ = Rx.Observable
        .forkJoin(...url$)
        .subscribe(results => {
          const n = results.reduce((ac, num) => ac += num)
          const factors = primeFactorList(n)
          res.send(`Prime factors for the sum of ${results.join(' and ')} are: ${factors.join(', ')}`)
        })
})
app.use('/',express.static("./dist/index.html"))

app.listen(3000, () => console.log('listening on port 3000'))
